<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\School as RequestSchool;

use App\Models\School;

use App\Repositories\SchoolRepository;
use Validator;

class SchoolsController extends Controller
{   
    private $SchoolRepository;
    public function __construct()
    {
        $this->middleware('auth');
        $this->SchoolRepository = new SchoolRepository();
    }
  
    public function index()
    {   
        return $this->SchoolRepository->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(RequestSchool $request)
    {   
        return $this->SchoolRepository->create($request);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        return $this->SchoolRepository->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        return $this->SchoolRepository->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->SchoolRepository->destroy($id);
    }
}
