<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\StudentsRepository;

class StudentsController extends Controller
{   

    private $StudentsRepository;
    public function __construct()
    {
        $this->middleware('auth');
        $this->StudentsRepository = new StudentsRepository();
    }
  

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($school_id)
    {
        return $this->StudentsRepository->index($school_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id)
    {
        return $this->StudentsRepository->create($request,$id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($school_id,$id)
    {
        return $this->StudentsRepository->edit($school_id,$id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $school_id,$id)
    {   
        return $this->StudentsRepository->update($request,$school_id,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($school_id,$id)
    {
        return $this->StudentsRepository->destroy($school_id,$id);
    }
}
