<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class School extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'web' => 'required|string|max:255',
            'address' => 'required',
            'image' => 'required|image|max:2000*2000*1|dimensions:max_height=200,max_width=200'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Este Campo es obligatorio',
            'image.required' => 'El Archivo es requerido',
            'image.image' => 'Formato no permitido. Debe ser imagen',
            'image.max' => 'El máximo permitido es 2 MB',
            'image.dimensions' => 'El logo debe ser tener una dimension maxima de 200x200',
        ];
    }
}
