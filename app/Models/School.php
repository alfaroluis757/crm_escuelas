<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{   
    use SoftDeletes;
    protected $fillable  = [
        'name', 'address','email','phone','url_img','path_img','web_page'
    ];

    public function students()
    {
        return $this->hasMany('App\Models\Student');
    }
}
