<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Student extends Model
{
    use SoftDeletes;
    protected $fillable  = [
        'name', 'lastname', 'city' , 'birth_date' , 'school_id'   
    ];

    public function school()
    {
        return $this->belongsTo('App\Models\School', 'school_id');
    }
}
