<?php

namespace App\Repositories;

use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use File;
use stdClass;
use Validator;

class FileRepository
{   

    public function save($request)
    {   
        $file = $request->file('image');
        $imageName = date("d")."-".date("m")."-".date("Y").Str::random(20)."-".date("h").'.'.$file->getClientOriginalExtension();
        $file->move(public_path().'/storage/',$imageName);
        $data = new stdClass;
        $data->path_img = '/storage/'.$imageName;
        $data->url_img = url('/storage/'.$imageName);
        
        return $data;
    }

  

}
