<?php

namespace App\Repositories;

use App\Models\School;

use App\Repositories\FileRepository;

use Validator;

class SchoolRepository
{   

    public function index()
    {
        $schools = School::paginate(5);
        return view('School.index',compact('schools'));
    }

    public function create($request)
    {   

        $FileRepository = new FileRepository();
        $file = $FileRepository->save($request);
    
        $data = [
            "name" => $request->name,
            "address" => $request->address,
            "email" => $request->email,
            "phone" => $request->phone,
            "url_img" => $file->url_img,
            "path_img" => $file->path_img,
            "web_page" => $request->web
        ];

        School::create($data);

        return redirect('/schools')->with('status','Se ha Agregado la escuela '.$request->name.' Correctamente');
    }

    public function destroy($id)
    {
        School::where('id',$id)->delete();

        return redirect('/schools')->with('danger','Se ha Eliminado la escuela Correctamente');;
    }

    public function edit($id)
    {   
        $school = School::find($id);
        return view('School.edit',compact('school'));
    }

    public function update($request,$id)
    {
        
        if($request->hasfile('image')){

            $rules = [
                'image' => 'required|image|max:2000*2000*1|dimensions:max_height=200,max_width=200'
            ];
            $messages = [
                'image.required' => 'El Archivo es requerido',
                'image.image' => 'Formato no permitido. Debe ser imagen',
                'image.max' => 'El máximo permitido es 2 MB',
                'image.dimensions' => 'El logo debe ser tener una dimension maxima de 200x200',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()){
                return redirect('/schools')->withErrors($validator);
            }
            
            $FileRepository = new FileRepository();
            $file = $FileRepository->save($request);

            $data = [
                "name" => $request->name,
                "address" => $request->address,
                "email" => $request->email,
                "phone" => $request->phone,
                "url_img" => $file->url_img,
                "path_img" => $file->path_img,
                "web_page" => $request->web
            ];
        }else{
            $data = [
                "name" => $request->name,
                "address" => $request->address,
                "email" => $request->email,
                "phone" => $request->phone,
                "web_page" => $request->web
            ];
        }        

        School::where('id',$id)->update($data);

        return redirect('/schools')->with('status','Se ha Actualizado la escuela '.$request->name.' Correctamente');
    }
}
