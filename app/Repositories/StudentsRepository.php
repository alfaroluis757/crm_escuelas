<?php

namespace App\Repositories;

use App\Models\Student;

class StudentsRepository
{   

    public function index($id)
    {   
        
        $Students = Student::where('school_id',$id)->paginate(5);      
        return view('Students.index',compact('Students','id'));
    }

    public function create($request,$id)
    {   
         $data = [
            "name" => $request->name,
            "lastname" => $request->lastname,
            "city" => $request->city,
            "birth_date" => $request->birth,
            "school_id" => $id         
        ];

        Student::create($data);

        return redirect('/schools/'.$id.'/students')->with('status','Se ha Agregado al estudiante '.$request->name.' Correctamente');
    }

    public function destroy($school_id,$id)
    {
        Student::where('id',$id)->delete();

        return redirect('schools/'.$school_id.'/students')->with('danger','Se ha Eliminado el Correctamente');;
    }

    public function edit($school_id,$id)
    {   
        $student = Student::find($id);
        return view('Students.edit',compact('student','school_id'));
    }

    public function update($request,$school_id,$id)
    {
        
        $data = [
            "name" => $request->name,
            "lastname" => $request->lastname,
            "city" => $request->city,
            "birth_date" => $request->birth         
        ];

        Student::where('id',$id)->update($data);

        return redirect('/schools/'.$school_id.'/students')->with('status','Se ha Actualizado el estudiante '.$request->name.' Correctamente');
    }
}
