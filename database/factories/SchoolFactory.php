<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\School;
use Faker\Generator as Faker;

$factory->define(School::class, function (Faker $faker) {
    $storage_path = public_path().'/storage/';
    $path = $faker->image($storage_path, $width = 200, $height = 200,null, false);
    return [
        'name' => $faker->company,
        'url_img' => url('/storage/'.$path),
        'path_img' => '/storage/'.$path,
        'email' => $faker->email, // password
        'phone' => $faker->phoneNumber,
        'web_page' => $faker->url,
        'address' => $faker->address
    ];
});
