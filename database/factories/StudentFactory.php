<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'lastname' => $faker->lastname, 
        'birth_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'city' => $faker->city,
        'school_id' => $faker->numberBetween($min = 1, $max = 10)
    ];
});
