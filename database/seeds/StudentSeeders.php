<?php

use Illuminate\Database\Seeder;

class StudentSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Student::class,100)->create();
    }
}
