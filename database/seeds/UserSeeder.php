<?php

use Illuminate\Database\Seeder;
use App\Models\User;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class)->create([
            'name' => 'Admin',
            'email'=> 'admin@technodac.com',
            'password' => Hash::make('password'),
        ]);
    }
}
