import login from './Views/Public/Login'

export const routes=[
    /* LOGIN y REGISTRO */ 
    {
        path: '/',
        name: 'login',
        component: login,
        meta: {
            auth: false,
        }
    }
]