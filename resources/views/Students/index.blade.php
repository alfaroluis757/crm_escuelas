@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Estudiantes <a href="/schools/{{$id}}/students/add" class="btn btn-primary float-right">Agregar</a></div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Apellido</th>
                                <th scope="col">Fecha de Nacimiento</th>
                                <th scope="col">Ciudad</th>
                                <th scope="col">Editar</th>
                                <th scope="col">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($Students as $key => $Student)
                            <tr>
                                <td>{{$Student->name}}</td>
                                <td>{{$Student->lastname}}</td>
                                <td>{{$Student->birth_date}}</td>
                                <td>{{$Student->city}}</td>
                               <td>
                                    <form action="{{route('EditStudent', [$id,$Student->id])}}" method="GET">
                                        <button type="submit" class="btn btn-primary">Editar</button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{route('DeleteStudent',[$id,$Student->id])}}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">
                                            <svg id="i-trash" viewBox="0 0 32 32" width="19" height="19" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                                <path d="M28 6 L6 6 8 30 24 30 26 6 4 6 M16 12 L16 24 M21 12 L20 24 M11 12 L12 24 M12 6 L13 2 19 2 20 6" />
                                            </svg>
                                        </button>
                                    </form>
                                </td>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <nav aria-label="Page navigation example">
                        {{$Students->links('layouts.pagination')}}
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection