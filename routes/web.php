<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SchoolsController@index');

Auth::routes([
    'register' => false, 
    'reset' => false, 
    'verify' => false, 
  ]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
  'prefix' => 'schools',
], function ($router) {
  Route::get('/', 'SchoolsController@index')->name('schools');
  
  Route::get('/add', function(){
    return view('School.add');
  })->name('ViewAddSchool');
  
  Route::post('/add_school', 'SchoolsController@create')->name('AddSchool');
  Route::get('/edit/{id}', 'SchoolsController@edit')->name('EditSchool');
  Route::post('/update/{id}', 'SchoolsController@update')->name('UpdateSchool');
  Route::delete('/delete/{id}', 'SchoolsController@destroy')->name('DeleteSchool');


  Route::group([
    'prefix' => '/{school_id}/students',
  ], function ($router) {
    Route::get('/', 'StudentsController@index')->name('Students');
    
    Route::get('/add', function($school_id){
      return view('Students.add')->with('id',$school_id);
    })->name('ViewAddStudent');
    
    Route::post('/add_student', 'StudentsController@create')->name('AddStudent');
    Route::get('/edit/{id}', 'StudentsController@edit')->name('EditStudent');
    Route::post('/update/{id}', 'StudentsController@update')->name('UpdateStudent');
    Route::delete('/delete/{id}', 'StudentsController@destroy')->name('DeleteStudent');
  });
  
  
});



